﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Windows;
using System.Windows.Controls;

namespace WpfApp
{
    public partial class MainWindow : Window
    {
        SqlConnection con;
        DataSet ds;
        List<string> listTables;

        public MainWindow()
        {
            InitializeComponent();

            con = new SqlConnection(ConfigurationManager.ConnectionStrings["Connect1"].ConnectionString);
            ds = new DataSet();
            listTables = new List<string>();
        }

        private void Table_Selected(object sender, RoutedEventArgs e)
        {
            SqlCommand s = new SqlCommand();
            TreeViewItem t = (TreeViewItem)sender;
            t.Items.Clear();

            if (t.Header.ToString() == "Таблиці")
            {
                s.CommandText = "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES where Not(TABLE_NAME like 'sysdiagrams')";
                s.Connection = con;
            }

            if (t.Header.ToString() == "Процедури")
            {
                s.CommandText = "SELECT name FROM sys.objects o WHERE o.[Type] = 'P' and not(name like 'sp%')";
                s.Connection = con;
            }

            con.Open();
            SqlDataReader R = s.ExecuteReader();

            if ((R.HasRows) & (t.Header.ToString() == "Таблиці"))
            {
                listTables = new List<string>();

                while (R.Read())
                {
                    TreeViewItem ts = new TreeViewItem();
                    ts.Header = R[0].ToString();
                    ts.Selected += Table_Selected1;
                    t.Items.Add(ts);

                    listTables.Add(R[0].ToString());
                }
            }

            if ((R.HasRows) & (t.Header.ToString() == "Процедури"))
            {
                while (R.Read())
                {
                    TreeViewItem ts = new TreeViewItem();
                    ts.Header = R[0].ToString();
                    ts.Selected += Proc_Selected;
                    t.Items.Add(ts);
                }
            }

            con.Close();
        }

        private void Table_Selected1(object sender, RoutedEventArgs e)
        {
            TreeViewItem t = (TreeViewItem)sender;

            if (ExistsTab(t.Header.ToString())) return;

            if (ds.Tables.Contains(t.Header.ToString()))
            {
                ds.Tables.Remove(t.Header.ToString());
            }

            SqlCommand s = new SqlCommand("SELECT * from " + t.Header.ToString(), con);
            con.Open();

            SqlDataAdapter DA = new SqlDataAdapter(s);
            DA.Fill(ds, t.Header.ToString());

            DataGrid d = new DataGrid();
            d.AutoGenerateColumns = true;
            d.ItemsSource = ds.Tables[t.Header.ToString()].DefaultView;

            AddTab(t.Header.ToString(), d);

            con.Close();
            Status.Text = "Завантажено таблицю " + t.Header.ToString();
        }

        private void Proc_Selected(object sender, RoutedEventArgs e)
        {
            TreeViewItem t = (TreeViewItem)sender;

            if (ExistsTab(t.Header.ToString())) return;

            SqlCommand s = new SqlCommand();
            s.CommandType = CommandType.StoredProcedure;
            s.CommandText = t.Header.ToString();
            s.Connection = con;

            switch (t.Header.ToString())
            {
                case "YoungestEntrepreneurInRegion":
                    ProcYoungestEntrepreneurInRegion(s, t.Header.ToString());
                    break;
                case "ListShopsProfilesOwnedByBusinessman":
                    ProcListShopsProfilesOwnedByBusinessman(s, t.Header.ToString());
                    break;
                case "TableDistinctValues":
                    ProcTableDistinctValues(s, t.Header.ToString());
                    break;
                case "InsertShop":
                    ProcInsertShop(s, t.Header.ToString());
                    break;
                case "UpdateShop":
                    ProcUpdateShop(s, t.Header.ToString());
                    break;
                case "DeleteShopById":
                    ProcDeleteShopById(s, t.Header.ToString());
                    break;
                default:
                    ProcWithoutParams(s, t.Header.ToString());
                    break;
            }

            Status.Text = "Відкрито вкладку процедури \"" + t.Header.ToString() + "\"";
        }

        private void ProcWithoutParams(SqlCommand sqlCommand, string procName)
        {
            Grid grid = new Grid();

            SetColumnsGrid(grid, 1);
            SetRowsGrid(grid);

            Button btn = GetExecBtn(grid, 0, 0);
            AddTab(procName, grid);

            btn.Click += (object sender, RoutedEventArgs e) =>
            {
                ExecProc(grid, sqlCommand, procName, 1, 1);
            };
        }

        private void ProcYoungestEntrepreneurInRegion(SqlCommand sqlCommand, string procName)
        {
            Grid grid = new Grid();

            SetColumnsGrid(grid);
            SetRowsGrid(grid, 3);

            TextBlock txtBlockRegion = GetTxtBlock(grid, "Район: ");
            TextBox txtBoxRegion = GetTxtBox(grid);

            Button button = GetExecBtn(grid);
            AddTab(procName, grid);

            button.Click += (object sender, RoutedEventArgs e) =>
            {
                string region = txtBoxRegion.Text;

                if (String.IsNullOrWhiteSpace(region))
                {
                    ShowErrorMessage("Введено некоректні дані");
                    return;
                }

                sqlCommand.Parameters.Clear();
                sqlCommand.Parameters.Add("@region", SqlDbType.VarChar, 32);
                sqlCommand.Parameters["@region"].Value = region;

                ExecProc(grid, sqlCommand, procName, 3);
            };
        }

        private void ProcListShopsProfilesOwnedByBusinessman(SqlCommand sqlCommand, string procName)
        {
            Grid grid = new Grid();

            SetColumnsGrid(grid);
            SetRowsGrid(grid, 3);

            TextBlock txtBlockLastName = GetTxtBlock(grid, "Прізвище: ");
            TextBox txtBoxLastName = GetTxtBox(grid);

            Button button = GetExecBtn(grid);
            AddTab(procName, grid);

            button.Click += (object sender, RoutedEventArgs e) =>
            {
                string lastName = txtBoxLastName.Text;

                if (String.IsNullOrWhiteSpace(lastName))
                {
                    ShowErrorMessage("Введено некоректні дані");
                    return;
                }

                sqlCommand.Parameters.Clear();
                sqlCommand.Parameters.Add("@last_name", SqlDbType.VarChar, 32);
                sqlCommand.Parameters["@last_name"].Value = lastName;

                ExecProc(grid, sqlCommand, procName, 3);
            };
        }

        private void ProcTableDistinctValues(SqlCommand sqlCommand, string procName)
        {
            Grid grid = new Grid();

            SetColumnsGrid(grid);
            SetRowsGrid(grid, 3);

            TextBlock txtBlockTableName = GetTxtBlock(grid, "Назва таблиці: ");
            TextBox txtBoxTableName = GetTxtBox(grid);

            Button button = GetExecBtn(grid);
            AddTab(procName, grid);

            button.Click += (object sender, RoutedEventArgs e) =>
            {
                string tableName = txtBoxTableName.Text;

                if (String.IsNullOrWhiteSpace(tableName))
                {
                    ShowErrorMessage("Введено некоректні дані");
                    return;
                }

                sqlCommand.Parameters.Clear();
                sqlCommand.Parameters.Add("@table_name", SqlDbType.VarChar, 128);
                sqlCommand.Parameters["@table_name"].Value = tableName;

                ExecProc(grid, sqlCommand, procName, 3);
            };
        }

        private void ProcInsertShop(SqlCommand sqlCommand, string procName)
        {
            Grid grid = new Grid();

            SetColumnsGrid(grid);
            SetRowsGrid(grid, 6);

            TextBlock txtBlockName = GetTxtBlock(grid, "Назва: ");
            TextBox txtBoxName = GetTxtBox(grid);

            TextBlock txtBlockRegion = GetTxtBlock(grid, "Район: ", 1);
            TextBox txtBoxRegion = GetTxtBox(grid, 1);

            TextBlock txtBlockProfile = GetTxtBlock(grid, "Профіль: ", 2);
            TextBox txtBoxProfile = GetTxtBox(grid, 2);

            TextBlock txtBlockCapital = GetTxtBlock(grid, "Капітал: ", 3);
            TextBox txtBoxCapital = GetTxtBox(grid, 3);

            Button button = GetExecBtn(grid, 4);
            AddTab(procName, grid);

            button.Click += (object sender, RoutedEventArgs e) =>
            {
                string name = txtBoxName.Text;
                string region = txtBoxRegion.Text;
                string profile = txtBoxProfile.Text;
                double capital;

                if (String.IsNullOrWhiteSpace(name) || String.IsNullOrWhiteSpace(region) ||
                String.IsNullOrWhiteSpace(profile) || !double.TryParse(txtBoxCapital.Text, out capital))
                {
                    ShowErrorMessage("Введено некоректні дані");
                    return;
                }

                sqlCommand.Parameters.Clear();
                sqlCommand.Parameters.Add("@name", SqlDbType.VarChar, 32);
                sqlCommand.Parameters["@name"].Value = name;

                sqlCommand.Parameters.Add("@region", SqlDbType.VarChar, 32);
                sqlCommand.Parameters["@region"].Value = region;

                sqlCommand.Parameters.Add("@profile", SqlDbType.VarChar, 32);
                sqlCommand.Parameters["@profile"].Value = profile;

                sqlCommand.Parameters.Add("@capital", SqlDbType.Decimal);
                sqlCommand.Parameters["@capital"].Precision = 9;
                sqlCommand.Parameters["@capital"].Scale = 2;
                sqlCommand.Parameters["@capital"].Value = capital;

                ExecProcIUD("I", sqlCommand, procName, "Shop");
            };
        }

        private void ProcUpdateShop(SqlCommand sqlCommand, string procName)
        {
            Grid grid = new Grid();

            SetColumnsGrid(grid);
            SetRowsGrid(grid, 7);

            TextBlock txtBlockShopId = GetTxtBlock(grid, "Shop id: ");
            TextBox txtBoxShopId = GetTxtBox(grid);

            TextBlock txtBlockName = GetTxtBlock(grid, "Назва: ", 1);
            TextBox txtBoxName = GetTxtBox(grid, 1);

            TextBlock txtBlockRegion = GetTxtBlock(grid, "Район: ", 2);
            TextBox txtBoxRegion = GetTxtBox(grid, 2);

            TextBlock txtBlockProfile = GetTxtBlock(grid, "Профіль: ", 3);
            TextBox txtBoxProfile = GetTxtBox(grid, 3);

            TextBlock txtBlockCapital = GetTxtBlock(grid, "Капітал: ", 4);
            TextBox txtBoxCapital = GetTxtBox(grid, 4);

            Button button = GetExecBtn(grid, 5);
            AddTab(procName, grid);

            button.Click += (object sender, RoutedEventArgs e) =>
            {
                int shopId;
                string name = txtBoxName.Text;
                string region = txtBoxRegion.Text;
                string profile = txtBoxProfile.Text;
                double capital;

                if (!int.TryParse(txtBoxShopId.Text, out shopId) ||
                String.IsNullOrWhiteSpace(name) || String.IsNullOrWhiteSpace(region) ||
                String.IsNullOrWhiteSpace(profile) || !double.TryParse(txtBoxCapital.Text, out capital))
                {
                    ShowErrorMessage("Введено некоректні дані");
                    return;
                }

                sqlCommand.Parameters.Clear();
                sqlCommand.Parameters.Add("@shop_id", SqlDbType.Int);
                sqlCommand.Parameters["@shop_id"].Value = shopId;

                sqlCommand.Parameters.Add("@name", SqlDbType.VarChar, 32);
                sqlCommand.Parameters["@name"].Value = name;

                sqlCommand.Parameters.Add("@region", SqlDbType.VarChar, 32);
                sqlCommand.Parameters["@region"].Value = region;

                sqlCommand.Parameters.Add("@profile", SqlDbType.VarChar, 32);
                sqlCommand.Parameters["@profile"].Value = profile;

                sqlCommand.Parameters.Add("@capital", SqlDbType.Decimal);
                sqlCommand.Parameters["@capital"].Precision = 9;
                sqlCommand.Parameters["@capital"].Scale = 2;
                sqlCommand.Parameters["@capital"].Value = capital;

                ExecProcIUD("U", sqlCommand, procName, "Shop");
            };
        }

        private void ProcDeleteShopById(SqlCommand sqlCommand, string procName)
        {
            Grid grid = new Grid();

            SetColumnsGrid(grid);
            SetRowsGrid(grid, 3);

            TextBlock txtBlockShopId = GetTxtBlock(grid, "Shop id: ");
            TextBox txtBoxShopId = GetTxtBox(grid);

            Button button = GetExecBtn(grid);
            AddTab(procName, grid);

            button.Click += (object sender, RoutedEventArgs e) =>
            {
                int shopId;

                if (!int.TryParse(txtBoxShopId.Text, out shopId))
                {
                    ShowErrorMessage("Введено некоректні дані");
                    return;
                }

                sqlCommand.Parameters.Clear();
                sqlCommand.Parameters.Add("@shop_id", SqlDbType.Int);
                sqlCommand.Parameters["@shop_id"].Value = shopId;

                ExecProcIUD("D", sqlCommand, procName, "Shop");
            };
        }

        // operationName == "I" - insert or
        //                  "U" - update or
        //                  "D" - delete
        private void ExecProcIUD(string operationName, SqlCommand sqlCommand,
            string procName, string tableName)
        {
            try
            {
                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter();
                
                switch (operationName)
                {
                    case "I":
                        DA.InsertCommand = sqlCommand;
                        DA.InsertCommand.ExecuteNonQuery();
                        break;
                    case "U":
                        DA.UpdateCommand = sqlCommand;
                        DA.UpdateCommand.ExecuteNonQuery();
                        break;
                    case "D":
                        DA.DeleteCommand = sqlCommand;
                        DA.DeleteCommand.ExecuteNonQuery();
                        break;
                    default:
                        return;
                }

                if (ds.Tables.Contains(tableName))
                {
                    DA.Update(ds, tableName);
                    ds.AcceptChanges();
                }

                Status.Text = "Успішно виконано процедуру " + procName;
            }
            catch (Exception ex)
            {
                Status.Text = "Виникла помилка під час виконання процедури " + procName;
                ShowErrorMessage(ex.Message);
            }
            finally
            {
                con.Close();
            }
        }

        private void ExecProc(Grid grid, SqlCommand sqlCommand,
            string procName, int gridRow, int gridColumnSpan = 2)
        {
            if (ds.Tables.Contains(procName))
            {
                ds.Tables.Remove(procName);
            }

            try
            {
                con.Open();

                SqlDataAdapter DA = new SqlDataAdapter(sqlCommand);
                DA.Fill(ds, procName);

                DataGrid d = new DataGrid();
                d.AutoGenerateColumns = true;
                d.ItemsSource = ds.Tables[procName].DefaultView;

                Grid.SetRow(d, gridRow);
                Grid.SetColumnSpan(d, gridColumnSpan);

                for (int i = 0; i < grid.Children.Count; i++)
                {
                    if (grid.Children[i] is DataGrid)
                    {
                        grid.Children.RemoveAt(i);
                        i--;
                    }
                }

                grid.Children.Add(d);

                con.Close();
            }
            catch (Exception ex)
            {
                ShowErrorMessage(ex.Message);
            }
            finally
            {
                con.Close();
            }

            Status.Text = "Виконано процедуру " + procName;
        }

        private void SetRowsGrid(Grid grid, int row = 2)
        {
            for (int i = 0; i < row - 1; i++)
            {
                grid.RowDefinitions.Add(new RowDefinition()
                {
                    Height = GridLength.Auto
                });
            }

            grid.RowDefinitions.Add(new RowDefinition());
        }

        private void SetColumnsGrid(Grid grid, int column = 2)
        {
            for (int i = 0; i < column - 1; i++)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition()
                {
                    Width = GridLength.Auto
                });
            }

            grid.ColumnDefinitions.Add(new ColumnDefinition());
        }

        private Button GetExecBtn(Grid grid, int row = 1, int column = 1)
        {
            Button button = new Button();
            button.Content = "EXECUTE";

            Grid.SetRow(button, row);
            Grid.SetColumn(button, column);
            grid.Children.Add(button);

            return button;
        }

        private TextBlock GetTxtBlock(Grid grid, string text, int row = 0, int column = 0)
        {
            TextBlock txtBlock = new TextBlock();
            txtBlock.Text = text;

            Grid.SetRow(txtBlock, row);
            Grid.SetColumn(txtBlock, column);
            grid.Children.Add(txtBlock);

            return txtBlock;
        }

        private TextBox GetTxtBox(Grid grid, int row = 0, int column = 1)
        {
            TextBox txtBox = new TextBox();

            Grid.SetRow(txtBox, row);
            Grid.SetColumn(txtBox, column);
            grid.Children.Add(txtBox);

            return txtBox;
        }

        private void AddTab(string tabHeader, object content)
        {
            ContextMenu c = new ContextMenu();
            MenuItem m = new MenuItem();
            m.Header = "Закрити";
            m.Click += MenuItem_Click;
            c.Items.Add(m);

            tabs.Items.Add(new TabItem
            {
                Header = tabHeader,
                Content = content,
                ContextMenu = c,
                IsSelected = true
            });
        }

        private bool ExistsTab(string tabHeader)
        {
            foreach (TabItem tabItem in tabs.Items)
            {
                if (tabItem.Header.ToString() == tabHeader)
                {
                    tabs.SelectedItem = tabItem;
                    Status.Text = "Відкрито вкладку \"" + tabHeader + "\"";
                    return true;
                }
            }

            return false;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            tabs.Items.RemoveAt(tabs.SelectedIndex);
        }

        private void ShowErrorMessage(string text)
        {
            MessageBox.Show(text, "Виникла помилка", MessageBoxButton.OK, MessageBoxImage.Error);
        }

        private void ButtonFirst_Click(object sender, RoutedEventArgs e)
        {
            if (tabs.Items.Count > 0)
                tabs.SelectedIndex = 0;
        }

        private void ButtonPrev_Click(object sender, RoutedEventArgs e)
        {
            if (tabs.SelectedIndex - 1 >= 0)
                tabs.SelectedIndex = tabs.SelectedIndex - 1;
        }

        private void ButtonNext_Click(object sender, RoutedEventArgs e)
        {
            if (tabs.SelectedIndex + 1 < tabs.Items.Count)
                tabs.SelectedIndex = tabs.SelectedIndex + 1;
        }

        private void ButtonEnd_Click(object sender, RoutedEventArgs e)
        {
            if (tabs.Items.Count - 1 > 0)
                tabs.SelectedIndex = tabs.Items.Count - 1;
        }

        private void ButtonUpdate_Click(object sender, RoutedEventArgs e)
        {
            TabItem t = (TabItem)tabs.SelectedItem;

            if (t != null && listTables.Contains(t.Header.ToString()))
            {
                con.Open();

                if (ds.Tables.Contains(t.Header.ToString()))
                {
                    ds.Tables.Remove(t.Header.ToString());
                }

                SqlCommand s = new SqlCommand("SELECT * from " + t.Header.ToString(), con);

                SqlDataAdapter DA = new SqlDataAdapter(s);
                DA.Fill(ds, t.Header.ToString());

                DataGrid d = (DataGrid)t.Content;
                d.ItemsSource = ds.Tables[t.Header.ToString()].DefaultView;

                con.Close();
                Status.Text = "Оновлено " + t.Header.ToString();
            }
        }
    }
}
