use Shops

create proc procShop as
	select * from Shop

exec procShop


create proc procOwner as
	select * from Owner

exec procOwner


create proc InsertShop
	@name varchar(32),
	@region varchar(32),
	@profile varchar(32),
	@capital decimal(9,2)
as begin
	insert into Shop(name, region, profile, capital)
	values (@name, @region, @profile, @capital)
end;

exec InsertShop 'Test name', 'Test region', 'Test profile', 1000.0
select * from Shop


create proc UpdateShop
	@shop_id int,
	@name varchar(32),
	@region varchar(32),
	@profile varchar(32),
	@capital decimal(9,2)
as begin
	update Shop set 
		name = @name,
		region = @region,
		profile = @profile,
		capital = @capital
	where id = @shop_id
end;

select * from Shop
exec UpdateShop 11, 'name', 'region', 'Test profile', 1000.0


create proc DeleteShopById
	@shop_id int
as begin
	delete from Shop
	where id = @shop_id
end;

select * from Shop
exec DeleteShopById 11